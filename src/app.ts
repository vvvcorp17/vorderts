import express, {Request, Response} from 'express';
import morgan from 'morgan';
import path from 'path';
import fileUpload from 'express-fileupload';
require('custom-env').env();

import User from './components/user/userObject';
import Log from './components/log/logObject';
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const expressip = require('request-ip');

const MongoStore = require('connect-mongo')(session);

// Routes
import Router from './controllers/Router';
import {Schema} from "mongoose";

class Application {
    app: express.Application;

    constructor() {
        this.app = express();
        this.app.use(cookieParser());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        this.settings();
        this.middlewares();
        this.routes();
    }

    settings() {
        this.app.set('port', process.env.PORT || 4000);
    }

    middlewares() {

        this.app.use((req, res, next) => {
            if(req.url !== '/' && req.url.indexOf("html") === -1) return next();
            let bot;
            ['Google', 'google', 'Yandex', 'yandex', 'vvv'].forEach(function(item){
                if (!bot && (req.headers['user-agent']).indexOf(item) > -1) {
                    console.log(req.url);
                    console.log(item);
                    bot = true;
                    return res.sendFile(__dirname + '/public/static/index.html');
                }
            });
            if(!bot) next();
        });
        this.app.use(function(req, res, next){

            let url = req.url.split("/")
            let lang = url[1];
            if(['ru', 'en', 'ro'].indexOf(lang) > -1) {
                req.url = req.url.replace('/'+lang, '/');
            }
            req['lang'] = lang || 'ru';
            next();
        });

        this.app.use(fileUpload({ safeFileNames: true, preserveExtension: true}));
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use(session({
            secret: 'a4f8071f-c245-4447-8ee2',
            cookie: { maxAge: 2628000000 },
            resave: true,
            saveUninitialized: false,
            store: new MongoStore({
                url: 'mongodb://localhost/session',
                ttl: 14 * 24 * 60 * 60 // = 14 days. Default
            })
        }));

        this.app.use(express.urlencoded({extended: false}));
        this.app.use(express.json());
        this.app.use(expressip.mw());

        this.app.use(
            morgan(function (tokens, req, res) {
                if(req.url === '/getNewMessagesCount') return;

                let message = {
                    user: req.session.user && req.session.user.email || 'Unauthorized',
                    ip: req.clientIp,
                    method: tokens.method(req, res),
                    url: tokens.url(req, res),
                    body: JSON.stringify(req.body),
                    status: tokens.status(req, res),
                    contentLength: tokens.res(req, res, 'content-length'),
                    responseTime: tokens['response-time'](req, res),
                };
                Log.add(message)
                console.log(JSON.stringify(message));
            }),


            async function (req: Request, res: Response, next) {

            if (!req.xhr && req.method === 'GET' && (req.url).indexOf('html') > -1) {
                return res.sendFile(__dirname + '/public/index.html');
            }

            if (req.session.user) {
                User.update(req.session.user._id, {lastActivity: new Date().toString()})
                    .then()
            }

            next();
        });
    }


    routes() {
        Router(this.app);
        this.app.use('*', (req, res) => {
            return res.status(404).sendFile(__dirname + '/public/pages/404.html');
        });
    }


    start(): void {
        this.app.listen(this.app.get('port'), () => {
            console.log('>>> Server is running at', this.app.get('port'));
        });
    }
}

export default Application;
