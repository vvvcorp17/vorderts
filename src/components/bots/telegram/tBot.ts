// Telegram bot
const TelegramBot = require('node-telegram-bot-api');

const token = '4bb4cecc23e7ddbd-333e332173e30c13-be560988bf5223c3';

const bot = new TelegramBot(token, {polling: true});

let users = [];
let steps = ['date', 'confirm', 'done'];

bot.onText(/\/start/, (msg) => {
    var options = {
        reply_markup: {
            "keyboard": [
                ["/categories", "/filter", "/myorders"],
                ["/help"],
            ]
        }
    };
    bot.sendMessage(msg.chat.id, "Welcome", options);
});

bot.onText(/\/help/, (msg) => {
    showAllCommands(bot, msg.chat.id)
});

bot.onText(/\/myorders/, (msg) => {
    let user = users[msg.chat.username] || [];

    let userOrders = user.length ? '' : 'No orders';
    user.forEach((item, index) => {
        if(item.confirm !== '1') return;
        index = index+1;
        userOrders += 'Order id: '+ index
            + '\nService: ' + item.serviceId
            + '\nto Date/Time: ' + item['to-date-time']
            + '\ncreated: ' + item.createdDate
            + '\n\n'
    });

    bot.sendMessage(msg.chat.id, "Your Orders \n" + userOrders);
});

bot.onText(/\/categories/, (msg) => {

    let buttons = [];

    // Categories.items()
    //     .then((items) => {
    //         items.forEach((item :any) => {
    //             buttons.push([{text: item.ruTitle, callback_data: '/services/category/'+item._id }])
    //         });
    //
    //         let options = {
    //             reply_markup: JSON.stringify({
    //                 inline_keyboard: buttons,
    //             })
    //         };
    //
    //         bot.sendMessage(msg.chat.id, "Categories", options);
    //     })
});

bot.on('callback_query', function onCallbackQuery(callbackQuery) {
    let data = callbackQuery.data;
    const msg = callbackQuery.message;
    data = data.split('/');

    if(data[1] === 'order') {
        let user = users[msg.chat.username];
        bot.sendMessage(msg.chat.id, " Order processing... ", );

        if(user && user.length) users[msg.chat.username].push({step: 0, serviceId: data[2]});
        else users[msg.chat.username] = [{step: 0, serviceId: data[2]}];

        bot.sendMessage(msg.chat.id, "Chose date/time ", );
    }
    if(data[1] === 'deatils') {
        // Services.item(data[3])
        //     .then((item) => {
        //         bot.sendPhoto(msg.chat.id, item.image,
        //             {
        //                 caption: '\nShort Desc: ' + item.ruSDesc
        //                 + '\n\nFull Desc: ' + item.ruFDesc,
        //                 reply_markup: JSON.stringify({
        //                     inline_keyboard: [[
        //                         {text: 'order', callback_data: '/order/' + item._id}]],
        //                 })
        //             });
        //     })
    }
    if(data[1] === 'services') {
        // Services.items({[data[2]]: data[3], populate: ['user', 'category']}, 'ru', 3)
        //     .then((items) => {
        //         let services = '';
        //         items.forEach((item) => {
        //             bot.sendPhoto(msg.chat.id, item.image,
        //                 {
        //                     caption: '\nTitle: ' + item.ruTitle
        //                     + '\nCategory: ' + item.category.ruTitle
        //                     + '\nPrice: ' + item.price.amount + ' ' + item.price.currency
        //                     + '\nOwner: ' + item.user.name + ' ' + item.user.email
        //                     + '\nRate: ' + item.rate.avgRate + '❇️ (' + item.rate.sumRate + ')',
        //                     reply_markup: JSON.stringify({
        //                         inline_keyboard: [[
        //                             {text: 'deatils', callback_data: '/deatils/service/' + item._id},
        //                             {text: 'order', callback_data: '/order/' + item._id}]],
        //                     })
        //                 });
        //         });
        //     })
    }
});

bot.on('message', (msg) => {
    let user = users[msg.chat.username];
    if(!user) return;

    let userOrderCount = user.length;
    if(user[userOrderCount-1].step === 0){
        user[userOrderCount-1].step =  1;
        user[userOrderCount-1]['to-date-time'] =  msg.text;
        bot.sendMessage(msg.chat.id, "confirm? 1 or 0", );
    } else if(user[userOrderCount-1].step === 1){
        user[userOrderCount-1].step =  2;
        user[userOrderCount-1]['confirm'] =  msg.text;
        user[userOrderCount-1]['createdDate'] =  new Date().toString();
        if(msg.text === '1')
            bot.sendMessage(msg.chat.id, "Complete! Order id is - " + userOrderCount);
        else
            bot.sendMessage(msg.chat.id, "Cancel");
    }
    // else {
    //     showAllCommands(bot, msg.chat.id)
    // }
});

function showAllCommands(bot, id) {
    bot.sendMessage(id,
        '\nAll Commands: '
        + '\n/start: '
        + '\n/categories: '
        + '\n/myorders: '
        + '\n/help: '
    )
}

//
// bot.on('inline_query', function onCallbackQuery(callbackQuery) {
//     const data = callbackQuery.data;
//     const msg = callbackQuery.message;
//     bot.sendMessage(msg.chat.id, 'inline_query ' + data)
// });

// bot.on('poll_answer', function onCallbackQuery(callbackQuery) {
//     console.log('poll_answer')
//     console.log(callbackQuery)
// });
// bot.on('poll', function onCallbackQuery(callbackQuery) {
//     console.log('poll')
//     console.log(callbackQuery)
// });

//
// bot.onText(/\/ques/, (msg) => {
//     bot.sendPoll(msg.chat.id, 'Ques?', ['opt 1', 'opt 2'], )
// });