import CategoryModel from "./models/categoryModel";

class CategoryObject {
    item(id): any {
        return CategoryModel.findById(id)
    }

    itemByColumn(find): any {
        return CategoryModel.findOne(find)
    }

    items() {
        return CategoryModel.find().sort({title: 1})
    }

    add(data) {
        let object = new CategoryModel(data);
        return object.save()
    }

    update(id, data) {
        return CategoryModel.findByIdAndUpdate(id, data)
    }

    delete(id) {
        return CategoryModel.findByIdAndDelete(id);
    }
}

export default new CategoryObject();

