import { Schema, model } from 'mongoose';

let CategorySchema = new Schema({
    ruTitle : {
        type: String,
        required: true,
        unique: true
    },
    roTitle : {
        type: String,
        unique: true
    },
    enTitle : {
        type: String,
        unique: true
    },
    image: {
        type: String
    }
});

export default model('Category', CategorySchema);
