import Model from './models/commentModel';


class ModelObject {

    item(id): any {
        return Model.findById(id);
    }

    itemByColumn(param): any {
        return Model.findOne(param);
    }

    items(find = {}, limit = 10, skip = 0) {

        return Model
            .find(find)
            .populate({path: 'user', select: ['name', 'email', 'image'] })
            .sort({_id: -1})
            .limit(limit)
            .skip(skip);
    }

    count(resourceType, resourceId) {
        return Model.find({resourceType, resourceId}).countDocuments();
    }

    add(data) {
        let object = new Model(data);
        return object.save();
    }

    delete(id) {
        return Model.findByIdAndDelete(id);
    }

    update(id, data) {
       return  Model.updateOne({_id: id}, data, {runValidators: true});
    }
}

export default new ModelObject();

