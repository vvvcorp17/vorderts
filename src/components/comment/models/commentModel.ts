import { Schema, model } from 'mongoose';
const TSchema = new Schema({
    resourceType: {
        type: String,
        enum: ['service', 'provider'],
        required: true
    },
    resourceId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    body: {
        type: String,
        required: true
    },
    approved: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date
    },
});

export default model('Comment', TSchema);
