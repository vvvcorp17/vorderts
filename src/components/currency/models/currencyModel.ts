import { Schema, model } from 'mongoose';

let CurrencySchema = new Schema({
    MDL: {
        type: Number
    },
    USD: {
        type: Number
    },
    EUR: {
        type: Number
    },
    RU: {
        type: Number
    }
});

export default model('Currency', CurrencySchema);
