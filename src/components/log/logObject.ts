// import ProviderModel from "../provider/models/providerModel";
// import ServiceModel from "../service/models/serviceModel";
import mongoose from 'mongoose';
import LogModel from "./models/logModel";


class LogObject {
    itemByColumn(param, limit = 25, skip = 0) {
        return LogModel.find(param).limit(limit).skip(skip).sort({_id: -1})
    }

    async add(data) {
        let object = await new LogModel(data);
        let order = await object.save();
        return order;
    }

}


export default new LogObject();

