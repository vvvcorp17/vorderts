import { Schema, model } from 'mongoose';

let LogSchema = new Schema({
    user: {type: String},
    ip: {type: String},
    method: {type: String},
    url: {type: String},
    body: {type: String},
    status: {type: Number},
    contentLength: {type: Number},
    responseTime: {type: Number},
}, { timestamps: true });

export default model('Log', LogSchema);
