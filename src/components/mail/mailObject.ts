import emailer from 'nodemailer';

class mailObject {
    send(to, from = 'Vorder App', subject, html): any {
        let transporter = emailer.createTransport({
                service: 'gmail',
                auth: {
                    user: process.env.from_email,
                    pass: process.env.email_password,
                }
            });

        let mailOptions = { from, to, subject, html };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent to: ' + 'user.email');
                console.log('Email sent: ' + info.response);
            }
        });
    }
}

export default new mailObject();
