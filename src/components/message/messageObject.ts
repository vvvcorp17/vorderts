import MessageModel from "./models/messageModel";
import xssFilters from "xss-filters";
import {Types} from "mongoose";
import User from "../user/userObject";

class MessageObject {
    item(id): any {
        return MessageModel.findById(id)
    }

    allNew(id) {
        return MessageModel.aggregate(
            [
                {"$match": {toUser: Types.ObjectId(id), read: false}},
                {"$group": {_id: "$fromUser", count: {$sum: 1}}}
            ]
            );
    }

    allNewCount(id) {
        return MessageModel
            .find({toUser: Types.ObjectId(id), read: false})
            .countDocuments();
    }


    allCompanions(id) {
        return MessageModel.aggregate(
            [
                { "$match": {$or: [ { fromUser: Types.ObjectId(id) }, { toUser: Types.ObjectId(id) } ]}},
                { "$group": { _id: {$cond: { if: { $eq: [ "$fromUser", Types.ObjectId(id) ] }, then: "$toUser", else: "$fromUser" }} } },
                { "$lookup": { from: "users", localField: "_id", foreignField: "_id", as: "user" } },
            ]
        )
            .limit(10)
        // return MessageModel.distinct('toUser',
        //     {$or: [ { fromUser: id }, { toUser: id } ]})
    }

    markRead(currentUser, companion) {
        return MessageModel.updateMany(
            {fromUser: companion, toUser: currentUser, read: false},
            {read: true}
        );
    }

    items(currentUser, companion) {

        this.markRead(currentUser, companion).then()

        return MessageModel.find(
            {$or: [{fromUser: currentUser, toUser: companion }, {fromUser: companion, toUser: currentUser}]})
            .sort({created_at: 1})
            .limit(20);

    }

    add(user, data) {
        data['fromUser'] = user;
        data.body = xssFilters.inHTMLData(data.body);
        let object = new MessageModel(data);

        User.pushNotify(data.toUser,
            {
                title: "New message from " + data.fromUser,
                body: data.body,
                url: "/profile/messages.html?companion=" + data.fromUser
                // actions: [{action: 'viewMessages', title: 'View New messages', url: "/profile/messages.html?companion=" + data.fromUser}]
            });

        return object.save()
    }

    update(id, data) {
        data.body = xssFilters.inHTMLData(data.body);
        return MessageModel.findByIdAndUpdate(id, data)
    }

    delete(currentUser, companion) {
        return MessageModel.remove({$or: [{fromUser: currentUser, toUser: companion }, {fromUser: companion, toUser: currentUser}]});
    }

    deleteAll() {
        return MessageModel.remove({});
    }
}

export default new MessageObject();

