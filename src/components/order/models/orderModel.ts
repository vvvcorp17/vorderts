import { Schema, model } from 'mongoose';

let OrderSchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    serviceOwner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    specialist: {
        type: Schema.Types.ObjectId,
        ref: 'Specialist'
    },
    name: {
        type: String,
        required:true
    },
    phone: {
        type: String,
        required:true
    },
    email: {
        type: String,
        required:true
    },
    created: {
        type: Date,
        required: true,
        default: Date
    },
    params: {
        toDataTime:{
            type: Date,
            required: true
        }
    },
    accepted: {
        type: Boolean,
        default: null
    },
    price: {
        currency: String,
        amount: Number,
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

export default model('Order', OrderSchema);
