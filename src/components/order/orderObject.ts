
import OrderModel from "./models/orderModel";
import User from "../user/userObject";
import Service from "../service/serviceObject";
import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;


class OrderObject {
    item(id): any {
        return OrderModel.findById(id)
    }

    itemByColumn(param) {
        return OrderModel.find(param)
    }

    items() {
        return OrderModel.find().populate({path: 'service', select: ['ruTitle','roTitle','enTitle'] }).sort({_id: -1})
    }

    myItems(user) {
        return OrderModel
            .find({user: user})
            .populate('specialist')
            .populate({path: 'service', select: ['ruTitle','roTitle','enTitle'] })
            .sort({_id: -1})
    }

    itemsByService(service) {
        return OrderModel.find({service: service})
    }

    itemsByServiceCount(service) {
        return OrderModel.find({service: service}).countDocuments()
    }

    allItems() {
        return OrderModel.find().sort({_id: -1})
    }


    clientItems(id) {
        return OrderModel
            .find({serviceOwner: id})
            .populate({path: 'service', select: ['ruTitle','roTitle','enTitle'] })
            .populate({path: 'user', select: ['name', 'email', 'image'] })
            .sort({isDeleted: 1, _id: -1})
    }

    count() {
        return OrderModel.find().countDocuments()
    }

    async add(data) {
        let service = await Service.item(data.service);

        let price = {
            amount: service.price.amount,
            currency: service.price.currency
        };

        data.serviceOwner = service.user;
        data.price = price;

        let object = await new OrderModel(data);
        let order = await object.save();

        User.getUserIdByServiceId(data.service)
            .then((service) => {
                User.notify(service.user, 'New Order', 'Order ID - ' + order._id);
            });

        return order;
    }

    delete(find) {
        return OrderModel.findOneAndUpdate(find, {isDeleted: true})
    }

    update(find, data) {
        return OrderModel.findOneAndUpdate(find, data, {runValidators: true})
    }
}


export default new OrderObject();

