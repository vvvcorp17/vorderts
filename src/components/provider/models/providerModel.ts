import {Schema, model} from 'mongoose';

let ProviderSchema = new Schema({
    ru : {
        title: {
            type: String,
            required: true
        },
        sDesc: {
            type: String,
            required: true
        },
        fDesc: {
            type: String,
            required: true
        },
    },
    // ro : {
    //     title: {
    //         type: String,
    //         required: true
    //     },
    //     sDesc: {
    //         type: String,
    //         required: true
    //     },
    //     fDesc: {
    //         type: String,
    //         required: true
    //     },
    // },
    // it : {
    //     title: {
    //         type: String,
    //         required: true
    //     },
    //     sDesc: {
    //         type: String,
    //         required: true
    //     },
    //     fDesc: {
    //         type: String,
    //         required: true
    //     },
    // },
    // en : {
    //     title: {
    //         type: String,
    //         required: true
    //     },
    //     sDesc: {
    //         type: String,
    //         required: true
    //     },
    //     fDesc: {
    //         type: String,
    //         required: true
    //     },
    // },
    mainImage: {
        type: String
    },
    images: [],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

export default model('Provider', ProviderSchema);
