import ProviderModel from "./models/providerModel";
import ServiceModel from "../service/models/serviceModel";
import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

class ProviderObject {
    item(id) {
        return ProviderModel.findById(id).populate({path: 'user', select: ['name', 'email', 'image'] });
    }

    itemsByColumn(param) {
        return ProviderModel.find(param).sort({_id: -1})
    }

    itemByColumn(param) {
        return ProviderModel.findOne(param)
    }

    items(limit = 25, skip = 0) {
        return ProviderModel.find().sort({_id: -1}).limit(limit).skip(skip)
    }

    count() {
        return ProviderModel.find().countDocuments()
    }

    add(data) {
        let object = new ProviderModel(data);
        return object.save()
    }


    pullImage(id, pull) {
        return ProviderModel.updateOne(
            {_id: ObjectId(id)},
            { $pull: { images: pull } },
            function (error, success) {
                if (error) {
                    console.log(error);
                }
            }
        );
    }

    async delete(id) {
        await ServiceModel.find({providerId: ObjectId(id)}).remove();
        return ProviderModel.findByIdAndDelete(id)
    }

    update(id, data) {
        return ProviderModel.findByIdAndUpdate(id, data, {runValidators: true})
    }
}

export default new ProviderObject();

