import { Schema, model } from 'mongoose';
const TSchema = new Schema({
    resourceType: {
        type: String,
        enum: ['service', 'provider', 'mainService'],
        required: true
    },
    resourceId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    body: {
        type: String,
        required: true
    },
    rate: {
        type: Number,
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date
    },
});

export default model('Review', TSchema);
