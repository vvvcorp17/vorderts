import Model from './models/reviewModel';
import Service from '../service/serviceObject';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

class ModelObject {

    item(id): any {
        return Model.findById(id);
    }

    itemByColumn(param): any {
        return Model.findOne(param);
    }

    itemsAvgRate(resourceId) {
        return Model.aggregate([
            {
                $match: { resourceId: ObjectId(resourceId)}
            },
            {
                $group:
                    {
                        _id: "$resourceId",
                        sumRate: {$sum: 1},
                        avgRate: { $avg: "$rate" }
                    }
            },
            { $limit : 1 }
        ])
    }

    items(find = {}, limit = 10, skip = 0) {
        return Model
            .find(find)
            .populate({path: 'user', select: ['name', 'email', 'image'] })
            .sort({_id: -1})
            .limit(limit)
            .skip(skip);
    }


    count() {
        return Model.find().countDocuments();
    }

    add(data) {
        let object = new Model(data);
        let obj = object.save();

        obj.then(() => {
            this.itemsAvgRate(data.resourceId)
                .then((res: any) => {
                    res = res[0];

                    Service.update({_id: res._id}, {rate: {sumRate: res.sumRate, avgRate: res.avgRate}})
                        .then()
                });
        });

        return obj;
    }

    delete(id) {
        return Model.findByIdAndDelete(id);
    }

    update(id, data) {
       return  Model.updateOne({_id: id}, data, {runValidators: true});
    }
}

export default new ModelObject();

