import {Schema, model, Document} from 'mongoose';
import {type} from "os";

export interface IService extends Document {
    user
    ruTitle
    ruSDesc
    ruFDesc
    workingDays
    startTime
    endTime
    startLunch
    endLunch
    category
    price
    image
    geoPosition
    rate
}

const ServiceSchema: Schema = new Schema({
    ruTitle: {
        type: String,
        required: true
    },
    ruSDesc: {
        type: String,
        required: true
    },
    ruFDesc: {
        type: String
    },
    roTitle: {
        type: String
    },
    roSDesc: {
        type: String
    },
    roFDesc: {
        type: String
    },
    enTitle: {
        type: String
    },
    enSDesc: {
        type: String
    },
    enFDesc: {
        type: String
    },
    price: {
        currency: {
            type: String,
            enum : ['MDL','EUR', 'USD', 'RUB'],
            default: 'USD'
        },
        amount: {
            type: Number,
            required: true
        }
    },
    viewCount: {
        type: Number,
        default: 0
    },
    publish: {
        type: Boolean,
        default: false
    },
    maxOrdersOnTime: {
        type: Number,
        default: 0
    },
    serviceTimeDuration: {
        type: Number,
        default: 0
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    geoPosition: {
        lat: Number,
        lng: Number,
        name: String
    },
    rate: {
        sumRate: {
            type: Number,
            default: 0
        },
        avgRate: {
            type: Number,
            default: 0
        },
    },
    workingDays: {
        type: Array,
        default: Array
    },
    specialists:[{
        type: Schema.Types.ObjectId,
        ref: 'Specialist'
    }],
    startTime:{
        type: String
    },
    endTime:{
        type: String
    },
    startLunch:{
        type: String
    },
    endLunch:{
        type: String
    },
    image: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, { timestamps: true });

export default model<IService>('Service', ServiceSchema);
