import ServiceModel from "./models/serviceModel";
import Currency from "../../components/currency/models/currencyModel";
import mongoose from 'mongoose';
import {removeImage} from "../../helpers/imageUpload";
import {EJSON} from "bson";

const ObjectId = mongoose.Types.ObjectId;


class ServiceObject {

    async amountInUserCurrency(Service, userCurrency) {

        let service = await Service;
        let currency = await Currency.findOne();

        // to MDL
        let amountInMDL = service.price.amount * currency[service.price.currency];

        // to userCurrency
        service.price.amountInUserCurrency = amountInMDL / currency[userCurrency];

        return service;
    }

    item(id, populate = []) {
        // let newPopulate = [];
        // populate && find.populate.forEach((item) => {
        //     if(item === 'user') newPopulate.push({path: 'user', select: ['name', 'email', 'image'] })
        //     if(item === 'category') newPopulate.push('category')
        //     if(item === 'specialists') newPopulate.push('specialists')
        // });

        ServiceModel.updateOne({_id: id}, {$inc: { viewCount: 1}}).then();
        return ServiceModel.findById(id)
            .populate('specialists')
            .populate('category')
            .populate({path: 'user', select: ['name', 'email', 'image'] });
    }

    itemWithCategory(id): any {
        return ServiceModel.aggregate([
            {$match: {_id: ObjectId(id)}},
            {
                $lookup: {
                    from: "categories",
                    localField: "category",
                    foreignField: "_id",
                    as: "category"
                }
            },
        ]).sort({_id: -1}).limit(100)
    }

    itemByColumn(param, limit = 4) {
        return ServiceModel.find(param).sort({_id: -1}).limit(limit)
    }

    count(find, lang, limit = null) {
        let filter: any = {};

        if (find._id)            filter._id = find._id;
        if (find.category)       filter.category = find.category;
        if (find.user)           filter.user = find.user;
        if (find.title)          filter[lang+"Title"] = { $regex: new RegExp(find.title,"i") };

        if (find.priceFrom || find.priceTo) {
            find.priceFrom  = find.priceFrom || 0;
            find.priceTo = find.priceTo || 99999999999;

            filter["$and"] = [
                {"price.amount": {$gte: find.priceFrom}}, {"price.amount": {$lte: find.priceTo}}
            ];
        }
        if (find.limit)          limit = Number(find.limit);

        filter.publish = true;
        if(find.user && find.reqUser === find.user) delete filter.publish;

        return ServiceModel.find(filter).limit(limit).countDocuments()
    }

    items(find: any = {}, lang = 'ru', limit: number = 9, skip = 0, sort: any = {}) {

        let filter: any = {};

        if (find.user)     filter.user = find.user;
        if (find.category) filter.category = find.category;
        if (find.title)    filter[lang+"Title"] = { $regex: new RegExp(find.title,"i") };

        // for not owner show only published
        filter.publish = true;
        if(find.user && find.reqUser === find.user) delete filter.publish;

        if (find.priceFrom || find.priceTo) {
            find.priceFrom  = find.priceFrom || 0;
            find.priceTo = find.priceTo || 99999999999;

            filter["$and"] = [
                {"price.amount": {$gte: find.priceFrom}}, {"price.amount": {$lte: find.priceTo}}
            ];
        }
        if (find.limit)          limit = Number(find.limit);

        sort = {_id : -1};
        if (find.priceDirection) sort = {"price.amount" : find.priceDirection == 'low' ? 1 : -1};

        // if(find.sort) {
        //     sort[find.sort] = -1;
        // } else sort.viewCount = -1;

        let page = find.page || 1;

        // let populate = ['user', 'category'];
        let populate = [];

        find.populate && find.populate.forEach((item) => {
            if(item === 'user') populate.push({path: 'user', select: ['name', 'email', 'image'] })
            if(item === 'category') populate.push('category')
        });

        return ServiceModel
            .find(filter, {
                ruFDesc: 0,
                roFDesc: 0,
                enFDesc: 0,
                createdAt: 0,
                specialists: 0,
                workingDays: 0,
                startTime: 0,
                endTime: 0,
                startLunch: 0,
                endLunch: 0,})
            .populate(populate)
            .sort(sort)
            .limit(limit)
            .skip((limit * page) - limit);
    }

    itemsGeo(find, limit = 10) {

        let findArray = [
            {"geoPosition.lat": {$gte: parseInt(find.southLat)}},
            {"geoPosition.lat": {$lte: parseInt(find.northLat)}},
            {"geoPosition.lng": {$gte: parseInt(find.southLng)}},
            {"geoPosition.lng": {$lte: parseInt(find.northLng)}},
        ];

        return ServiceModel
            .aggregate([
                {$match:
                    {$and: findArray}
                },
            ])
            .sort({_id: -1})
            .limit(limit);
    }

    add(data) {
        let object = new ServiceModel(data);
        return object.save()
    }

    delete(id) {
        ServiceModel.findById(id)
            .then(item => {
                if(item.image) {
                    removeImage(item.image)
                }
            });
        return ServiceModel.findByIdAndDelete(id)
    }

    pullImage(id, pull) {
        return ServiceModel.updateOne(
            {_id: ObjectId(id)},
            { $pull: { images: pull } },
            function (error, success) {
                if (error) {
                    console.log(error);
                }
            }
        );
    }

    update(id, data) {
        return ServiceModel.findOneAndUpdate({_id: id}, data, {runValidators: true});
    }
}

export default new ServiceObject();
