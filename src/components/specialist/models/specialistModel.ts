import {Schema, model, Document} from 'mongoose';

const SpecialistSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    position: {
        type: String
    },
    description: {
        type: String
    },
    email: {
        type: String,
        validate: {
            validator: function(v) {
                return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v);
            },
            message: props => `${props.value} is not a valid email address!`
        },
    },
    phone: {
        type: Number
    },
    serviceProvider: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created_at: {
        type: Date,
        default: Date
    },
    endPoint: {
        type: String,
        default: null
    },
    emailNotify: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

export default model('Specialist', SpecialistSchema);
