import Specialist from "./models/specialistModel";
import Service from "../service/serviceObject";
import Order from "../order/orderObject";
import emailer from 'nodemailer';
import {removeImage} from "../../helpers/imageUpload";


class SpecialistObject {

    item(id) {
        return Specialist.findById(id)
    }

    itemByColumn(param) {
        return Specialist.findOne(param)
    }

    itemsByColumn(param) {
        return Specialist.find(param)
    }

    items(find = {}, limit = 0, skip = 0) {
        return Specialist.find(find).sort({_id: -1}).limit(limit).skip(skip)
    }

    count() {
        return Specialist.find().countDocuments()
    }

    add(data) {
        let object = new Specialist(data);
        return object.save();
    }

    delete(id, serviceProvider) {
        this.deleteImageFromStore(id)
        return Specialist.deleteOne({_id: id, serviceProvider})
    }

    deleteImageFromStore(id) {
        Specialist.findById(id)
            .then(item => {
                if(item['image']) {
                    removeImage(item['image'])
                }
            });
    }

    update(id, serviceProvider, data) {

        if(data.image) {
            this.deleteImageFromStore(id)
        }

        let user = Specialist.updateOne({_id: id, serviceProvider}, data, {runValidators: true});

        return user;
    }

    async emailNotify(id, title, body) {

        let user = await this.item(id);

        if(user['emailNotify'] === 'false') return false;

        let transporter = emailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'neoninza13@gmail.com',
                pass: '029855005'
            }
        });

        let mailOptions = {
            from: 'neoninza13@gmail.com',
            to: user['email'],
            subject: title || 'Sending Email using Node.js',
            text: body || 'That was easy!'
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent to: ' + user['email']);
                console.log('Email sent: ' + info.response);
            }
        });
    }

    notify(id, title = '', body = '') {
        this.emailNotify(id, title, body);
    }

}

export default new SpecialistObject;

