import {Schema, model, Document} from 'mongoose';

const SubscribeSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    verifyHash: {
        type: String
    },
    verified: {
        type: Boolean,
        default: false
    }
},  { timestamps: true });

export default model('Subscriber', SubscribeSchema);
