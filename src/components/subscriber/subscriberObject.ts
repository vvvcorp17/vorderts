import Subscribe from "./models/subscriberModel";
import xssFilters from "xss-filters";
import mail from '../../components/mail/mailObject';

class SubscriberObject {

    items(find:any = {}, limit = 0, skip = 0) {
        return Subscribe.find(find).sort({_id: -1}).limit(limit).skip(skip)
    }

    count() {
        return Subscribe.find().countDocuments()
    }

    add(data:any) {
        data.email = xssFilters.inHTMLData(data.email);
        let object = new Subscribe(data);
        return object.save()
            .then(() => {
                mail.send(
                    data.email,
                    "Vorder.online",
                    "Подтверждение подписки на Vorder.online",
                    "Чтобы подвердить подписку, перейдите по ссылке <a href='"+ process.env.PROJECT_URL +"/subscribe/'"+ data.verifyHash +">подтвердить</a>>");
            })
    }

    verify(string) {
        return Subscribe.updateOne({verifyHash: string}, {verified: true}, {runValidators: true});
    }

    delete(id) {
        return Subscribe.findByIdAndDelete(id);
    }
}

export default new SubscriberObject;

