import { Schema, model } from 'mongoose';

let LogSchema = new Schema({
    timeStamp: {
        type: Date,
        default: Date
    },
    action: {
        type: String
    },
    params: {
        type: Object
    }

});

export default model('SysLog', LogSchema);
