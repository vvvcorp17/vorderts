// import ProviderModel from "../provider/models/providerModel";
// import ServiceModel from "../service/models/serviceModel";
import LogModel from "./models/sysLogModel";


class LogObject {

    items(limit = 25, skip = 0) {
        return LogModel.find({}).limit(limit).skip(skip).sort({_id: -1})
    }

    itemByColumn(param, limit = 25, skip = 0) {
        return LogModel.find(param).limit(limit).skip(skip).sort({_id: -1})
    }

    async add(data) {
        let object = await new LogModel(data);
        return await object.save();
    }
}

export default new LogObject();

