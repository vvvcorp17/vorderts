import {Schema, model, Document} from 'mongoose';

export interface OIUser extends Document {
    name : String,
    email : String
}

export interface IUser extends Document {
    name : String,
    email : String,
    phone : String,
    image : String,
    role : String,
    address : String,
    social : Object,
    password : String,
    created_at : Date,
    endPoint : String,
    emailNotify : Boolean,
    lastActivity : Date,
}

const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    role: {
        type: String,
        enum: ['user', 'provider', 'admin'],
        default: 'user'
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: Number,
    },
    address: {
        type: String
    },
    social: {
        type: {}
    },
    password: {
        type: String,
        required: true
    },
    recoveryLink: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date
    },
    lastActivity: {
        type: Date,
    },
    endPoint: {
        type: String,
        default: null
    },
    serviceProvider: {
        type: Boolean,
        default: false
    },
    emailNotify: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

export default model<IUser>('User', UserSchema);
