import webPush from 'web-push';
import UserModel, {IUser} from "./models/userModel";
import Service from "../service/serviceObject";
import Order from "../order/orderObject";
import emailer from 'nodemailer';
import {removeImage} from "../../helpers/imageUpload";
import xssFilters from "xss-filters";


// webPush credentials
const publicVapidKey = process.env.PUBLIC_VAPID_KEY;
const privateVapidKey = process.env.PRIVATE_VAPID_KEY;

webPush.setVapidDetails('mailto:neoninza3@gmail.com', publicVapidKey, privateVapidKey);


class UserObject {

    item(id): any {
        return UserModel.findById(id)
    }

    itemByColumn(param): any {
        return UserModel.findOne(param)
    }

    itemsByColumn(param) {
        return UserModel.find(param)
    }

    items(find:any = {}, limit = 0, skip = 0) {

        if (find.email)          find.email = { $regex: new RegExp(find.email, "i") };

        return UserModel.find(find).sort({_id: -1}).limit(limit).skip(skip)
    }

    count() {
        return UserModel.find().countDocuments()
    }

    add(data:any) {
        data.name = xssFilters.inHTMLData(data.name);
        data.email = xssFilters.inHTMLData(data.email);

        let object = new UserModel(data);
        return object.save();
    }

    delete(id) {
        this.deleteImageFromStore(id)
        return UserModel.findByIdAndDelete(id)
    }

    deleteImageFromStore(id) {
        UserModel.findById(id)
            .then(item => {
                if(item['image']) {
                    removeImage(item['image'])
                }
            });
    }

    update(id, data) {

        if (!data.emailNotify) data.emailNotify = false;

        if (data.name) data.name = xssFilters.inHTMLData(data.name);
        if (data.email) data.email = xssFilters.inHTMLData(data.email);

        let user = UserModel.updateOne({_id: id}, data, {runValidators: true});

        return user;
    }

    updateByColumn(find, data) {

        let user = UserModel.updateOne(find, data, {runValidators: true});

        return user;
    }

    subscribe(id, endpoint) {
        return UserModel.updateOne({_id: id}, {endPoint: endpoint})
    }

    getUserIdByServiceId(serviceId){
        return Service.item(serviceId);
    }

    getUserIdByOrderId(orderId){
        return Order.item(orderId);
    }

    async pushNotify(id, pushOptions) {
        let user:any = await UserModel.findById(id);

        if (user.endPoint) {
            // @ts-ignore
            webPush.sendNotification(JSON.parse(user.endPoint), JSON.stringify(pushOptions))
                .then(e => e)
                .catch((err) => {console.log('webpush err ' + err)});
        }
    }

    async emailNotify(id, title, body) {

        let user = await this.item(id);

        if(user.emailNotify === 'false') return false;

        let transporter = emailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'neoninza13@gmail.com',
                pass: '029855005'
            }
        });

        let mailOptions = {
            from: 'neoninza13@gmail.com',
            to: user.email,
            subject: title || 'Sending Email using Node.js',
            text: body || 'That was easy!'
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent to: ' + user.email);
                console.log('Email sent: ' + info.response);
            }
        });
    }

    notify(id, title = '', body = '') {
        this.pushNotify(id, {title, body});
        this.emailNotify(id, title, body);
    }

}

export default new UserObject;

