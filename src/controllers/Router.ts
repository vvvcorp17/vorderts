import homeRoutes from './home/homeRouter';
import seedRoutes from './seeds/seedRouter';
import orderRoutes from './order/orderRouter';
import categoryRoutes from './category/categoryRouter';
import commentRoutes from './comment/commentRouter';
import reviewRoutes from './review/reviewRouter';
import serviceRoutes from './service/serviceRouter';
import profileRoutes from './profile/profileRouter';
import userRoutes from './users/userRouter';
import specialistRoutes from './specialist/specialisRouter';
import messageRoutes from './message/messageRouter';
import commonRoutes from './common/commonRouter';


 let Router = (app) => {
    app.use('/users', userRoutes);
    app.use('/specialists', specialistRoutes);
    app.use('/profile', profileRoutes);
    app.use('/services', serviceRoutes);
    app.use('/categories', categoryRoutes);
    app.use('/comments', commentRoutes);
    app.use('/reviews', reviewRoutes);
    app.use('/orders', orderRoutes);
    app.use('/seed', seedRoutes);
    app.use('/messages', messageRoutes);
    app.use('/admin', commonRoutes);
    app.use('/', homeRoutes);
};



export default Router;