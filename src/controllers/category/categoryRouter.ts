import {Router, Request, Response, NextFunction} from 'express';
import Category from '../../components/category/categoryObject';
import auth from '../../helpers/authMiddleWare';
const router = Router();
import {uploadImage, uploadImages, removeImage} from '../../helpers/imageUpload'

// access for Logged only
router.route('/')
    .get(async (req: Request, res: Response, next: NextFunction) => {
        Category.items()
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    })
    .post(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let image;
        //@ts-ignore
        if(req.files && req.files.image) {
            //@ts-ignore
            image = uploadImage(req.files.image, 'categories');
        }

        Category.add({ruTitle: req.body.ruTitle, image})
            .then((item) => res.json(item))
            .catch((err) => res.json({'err': err}));
    });

router.route('/:id')
    .get(async (req: Request, res: Response, next: NextFunction) => {
        Category.item(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    })
    .put(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        Category.update(req.params.id, {ruTitle: req.body.ruTitle})
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    })
    .delete(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        Category.delete(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    });

export default router;
