import {Router, Request, Response, response} from 'express';
const router = Router();
import auth, {Auth} from '../../helpers/authMiddleWare';

import Comment from "../../components/comment/CommentObject";
// import Provider from "../../components/provider/providerObject";
// import Service from "../../components/service/serviceObject";
// import User from "../../components/user/userObject";
// import {removeImage} from "../../components/helpers/imageUpload";
import xssFilters from "xss-filters";


router.route('/:resourceType/:resourceId')
    .get((req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        let {limit, skip} = req.query;

        Comment.items({resourceType, resourceId}, 10, 0)
            .then((items) => {
                return res.json(items)
            })
            .catch((err) => res.json(err))
    })
    .post(auth.logged, (req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        let {body} = req.body;
        // let body = xssFilters.inHTMLData(message);

        Comment.add({resourceType, resourceId, body, user: req.session.user._id})
            .then((response) => res.json("Success"))
            .catch((err) => res.json(err))

    });

router.route('/:resourceType/:resourceId/count')
    .get((req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        Comment.count(resourceType, resourceId)
            .then((items) => {
                return res.json(items)
            })
            .catch((err) => res.json(err))
    });

// router.route('/:id')
//     .put(auth.commentOwner, (req: Request, res: Response) => {
//         console.log('put')
//
//     })
//     .delete(auth.commentOwner, (req: Request, res: Response) => {
//         Comment.delete(req.params.id)
//             .then(() => res.send('success'))
//     });

export default router;
