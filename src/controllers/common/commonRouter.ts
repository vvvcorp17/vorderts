import {Router, Request, Response, NextFunction} from 'express';
import auth from '../../helpers/authMiddleWare';
import categories from '../../components/category/categoryObject';
import specialists from '../../components/specialist/specialistObject';
import comments from '../../components/comment/CommentObject';
import services from '../../components/service/serviceObject';
import messages from '../../components/message/messageObject';
import users from '../../components/user/userObject';
import reviews from '../../components/review/reviewObject';
import orders from '../../components/order/orderObject';
import subscribers from '../../components/subscriber/subscriberObject';
const router = Router();

const fs = require('fs'),
    path = require('path'),
    filePathEn = path.join(__dirname, '../../public/assets/js/translate/en.js'),
    filePathRu = path.join(__dirname, '../../public/assets/js/translate/ru.js'),
    filePathRo = path.join(__dirname, '../../public/assets/js/translate/ro.js');

let resources = {
    'services' : services,
    'specialists' : specialists,
    'comments' : comments,
    'categories' : categories,
    'messages' : messages,
    'users' : users,
    'reviews' : reviews,
    'orders' : orders,
    'subscribers' : subscribers,
};

function getTranslate() {
    let translateAll:any = {};

    let data = fs.readFileSync(filePathEn, {encoding: 'utf-8'});
    eval(data);
    data = fs.readFileSync(filePathRu, {encoding: 'utf-8'});
    eval(data);
    data = fs.readFileSync(filePathRo, {encoding: 'utf-8'});
    eval(data);

    return translateAll;
}

function writeTranslate(translateAll) {
    fs.writeFile(filePathEn, "translateAll.en =" + JSON.stringify(translateAll.en), function (err) {
        if (err) return console.log(err);
    });
    fs.writeFile(filePathRo, "translateAll.ro =" + JSON.stringify(translateAll.ro), function (err) {
        if (err) return console.log(err);
    });
    fs.writeFile(filePathRu, "translateAll.ru =" + JSON.stringify(translateAll.ru), function (err) {
        if (err) return console.log(err);
    });
}

router.route('/lang')
    .get(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        if(req.hostname !== 'localhost') return res.end('from Local env only');

        res.json(getTranslate());
    })
    .post(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        if(req.hostname !== 'localhost') return res.end('from Local env only');

        let translateAll = getTranslate();
        let {index, ru, ro, en} = req.body;

        translateAll.ru[index] = ru;
        translateAll.ro[index] = ro;
        translateAll.en[index] = en;

        writeTranslate(translateAll);

        res.end();
    })
    .delete(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        if(req.hostname !== 'localhost') return res.end('from Local env only');

        let translateAll = getTranslate();
        let index = req.body.index;

        delete translateAll.en[index];
        delete translateAll.ru[index];
        delete translateAll.ro[index];

        writeTranslate(translateAll);
        res.end()
    });



// access for Logged only
router.route('/:resource')
    .get(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let resource = req.params.resource;
        resources[resource].items(req.query)
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    })
    .post(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let resource = req.params.resource;
        resources[resource].add(req.body)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

router.route('/:resource/:id')
    .get(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let resource = req.params.resource;
        resources[resource].item(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.status(404).json(err))
    })
    .put(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let resource = req.params.resource;
        resources[resource].update(req.params.id, req.body)
            .then((items) => res.json(items))
            .catch(err => res.status(404).json(err))
    })
    .delete(auth.admin, async (req: Request, res: Response, next: NextFunction) => {
        let resource = req.params.resource;
        resources[resource].delete(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    });

export default router;
