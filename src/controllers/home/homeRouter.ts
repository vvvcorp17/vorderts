import {Request, Response, Router} from 'express';
import bcrypt from 'bcryptjs';
import {Auth} from '../../helpers/authMiddleWare';
import mail from '../../components/mail/mailObject';
import User from "../../components/user/userObject";
import Subscriber from "../../components/subscriber/subscriberObject";
import {IUser} from "../../components/user/models/userModel";

const router = Router();

router.get('/check-auth', (req: Request, res: Response) => {
    if(req.session.user) {
        User.item(req.session.user._id).then((user) => res.json(user))
    } else {
        res.status(403).json()
    }
});

router.get('/get-header', async (req: Request, res: Response) => {
    res.render('layouts/navbar');
});

router.route('/recovery/link')
    .post(async (req: Request, res: Response) => {
        let recoveryLink = req.query.link;
        let {password} = req.body;

        User.itemByColumn({recoveryLink})
            .then((user) => {
                if(user) {
                    bcrypt.hash(password, 10, function(err, hash) {
                        User.update(user._id, {password: hash})
                            .then((item) => {
                                res.send("Success")
                            })
                            .catch((err) => res.status(422).json({error: err}))
                    });
                } else res.send("Not Found")
            })
    })

router.route('/recovery')
    .get(async (req: Request, res: Response) => {
        console.log(req.query)
    })
    .post(async (req: Request, res: Response) => {
        let {email} = req.body;
        let recoveryLink = Math.random().toString(36).substring(5);

        User.updateByColumn({email}, {recoveryLink})
            .then(() => {
                mail.send(email, null, 'Vorder Recovery password', `<a href="${process.env.PROJECT_URL}/recovery.html?link=${recoveryLink}"> Reset Password Link </a>`)
                res.send("Success");
            })
    });


router.get('/subscribe/:hash', async (req: Request, res: Response) => {
    Subscriber.verify(req.params.hash)
        .then((response) => res.send('success'))
        .catch((err) => res.status(404).json(err))
});

router.post('/subscribe', async (req: Request, res: Response) => {
    let {email} = req.body;
    let verifyHash = Math.random().toString(36).substring(7);
    Subscriber.add({email, verifyHash})
        .then((response) => res.json('success'))
        .catch((err) => res.status(422).json(err))
});

router.post('/login', async (req: Request, res: Response) => {
    let {email, password} = req.body;

    let user = await User.itemByColumn({email});

    if(!user) return res.status(422).send('Invalid Login or password');

    await bcrypt.compare(password, user.password, function(err, result) {
        if (result) {
            req.session.user = user;

            return res.send('Success');
        }

        else
            return res.status(422).send('Invalid Login or password');
    });

});

router.post('/register', async (req: Request, res: Response) => {
    let user: IUser = req.body;

    bcrypt.hash(user.password, 10, function(err, hash) {
        user.password = hash;

        User.add(user)
            .then((item) => {
                req.session.user = item;
                res.json(item)
            })
            .catch((err) => res.status(422).json({error: err}))
    });

});

// router.post('/authorize', async (req: Request, res: Response) => {
//     await Auth.check(req.query.token)
//         .then(async (response) => {
//             if (response === 'true') {
//
//                 //@ts-ignore
//                   let userData = JSON.parse(
//                         Buffer.from(
//                             req.query.token,
//                             'base64'
//                         ).toString()
//                     );
//                 console.log('currentUserData');
//
//
//                 let currentUserData = await User.itemByColumn({email: userData.email});
//
//                 if(!currentUserData) {
//                     currentUserData = await User.add(userData);
//                     currentUserData = await User.itemByColumn({email: userData.email});
//                 }
//
//                 req.session.user = currentUserData;
//
//                 return res.end('ok');
//             }
//         });
//     return res.end('error');
// });

// router.post('/authorize/social', async (req: Request, res: Response) => {
//     let errors;
//
//     let token = req.body.token;
//     token = JSON.stringify({email: req.body.email, name:req.body.name, token: token});
//     token = new Buffer(token).toString('base64');
//     let data = await User.add(token)
//         .then()
//         .catch((err) => {
//             errors = JSON.stringify(err);
//         });
//
//     if(errors) {
//         res.send(errors);
//         return;
//     }
//
//     res.cookie('auth', token, { maxAge: 48 * 60 * 60 * 1000 });
//     // res.cookie('auth', token, { maxAge: 48 * 60 * 60 * 1000, httpOnly: true });
//     return res.send('ok');
// });

router.get('/logout', async (req: Request, res: Response) => {
    delete req.session.user;
    res.json('ok');

});


export default router;
