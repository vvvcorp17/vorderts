import {Router, Request, Response, NextFunction} from 'express';
import Message from '../../components/message/messageObject';
import auth from '../../helpers/authMiddleWare';
const router = Router();

// access for Logged only
router.route('/')
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.add(req.session.user._id, req.body)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

router.route('/getNewMessages')
    .get(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.allNew(req.session.user._id)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

router.route('/getNewMessagesCount')
    .get(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.allNewCount(req.session.user._id)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

router.route('/companions')
    .get(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.allCompanions(req.session.user._id)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

// router.route('/all')
//     .delete(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
//         Message.deleteAll()
//             .then((items) => res.json(items))
//             .catch(err => res.status(400).json(err))
//     });

router.route('/:id')
    .get(auth.logged, async (req: Request, res: Response, next: NextFunction) => {

        Message.items(req.session.user._id, req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    })
    .put(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.update(req.params.id, {ruTitle: req.body.ruTitle})
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    })
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        Message.delete(req.session.user._id, req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    });

export default router;
