import {Router, Request, Response} from 'express';
import auth from '../../helpers/authMiddleWare';
import Order from "../../components/order/orderObject";

const router = Router();

router.route('/')
    .post((req: Request, res: Response) => {
        let {serviceId, toDataTime, name, phone, email, specialist} = req.body;
        const user = req.session.user && req.session.user._id || null;
        Order.add({
            service: serviceId,
            user, specialist,
            name, phone, email,
            params:{ toDataTime : toDataTime
            }
        })
            .then(() => res.json({status: 'ok'}))
            .catch((err) => res.status(400).json(err))
    });

router.route('/myOrders')
    .get((req: Request, res: Response) => {
        Order.myItems(req.session.user._id)
            .then((items) => res.json(items))
            .catch((err) =>  res.status(400).json({status: 'error', error: err}))
    });

router.route('/clientOrders')
    .get((req: Request, res: Response) => {
        Order.clientItems(req.session.user._id)
            .then((items) => res.json(items))
            .catch((err) =>  res.status(400).json({status: 'error', error: err}))
    });

router.route('/service/:id/count')
    .get((req: Request, res: Response) => {
        const {id} = req.params;
        Order.itemsByServiceCount(id)
            .then((item) => res.json(item))
            .catch((err) =>  res.status(400).json({status: 'error', error: err}))
    });

router.route('/accept/:id')
    .post((req: Request, res: Response) => {
        const {id} = req.params;
        Order.update({_id: id, serviceOwner: req.session.user._id}, {accepted: true})
            .then((items) => res.json(items))
            .catch((err) =>  res.status(400).json({status: 'error', error: err}))
    });

router.route('/decline/:id')
    .post((req: Request, res: Response) => {
        const {id} = req.params;
        Order.update({_id: id, serviceOwner: req.session.user._id}, {accepted: false})
            .then(() => {
                res.json('ok');
            })
            .catch((err) => res.status(400).json({status: 'error', error: err}))
    });

// router.route('/:id')
//     .delete((req: Request, res: Response) => {
//         const {id} = req.params;
//         Order.delete({_id: id, serviceOwner: req.session.user._id})
//             .then(() => {
//                 res.json('ok');
//             })
//             .catch((err) => res.status(400).json({status: 'error', error: err}))
//     });



export default router;
