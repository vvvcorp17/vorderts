import {Router, Request, Response, json} from 'express';
import auth from '../../helpers/authMiddleWare';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

import Service from "../../components/service/serviceObject";
import Order from "../../components/order/orderObject";
import User from "../../components/user/userObject";
import {removeImage, uploadImage} from "../../helpers/imageUpload";
import bcrypt from 'bcryptjs';

const router = Router();

router.route('/services')
    .get(auth.logged, (req: Request, res: Response) => {
        return Service.items({user: req.session.user._id})
            .then(item => res.json(item));
    });

// todo we need it ???
router.route('/services/:id')
    .get(auth.serviceOwner, (req: Request, res: Response) => {
        return Service.item(req.params.id)
            .then(item => res.json(item));
    });


router.route('/settings')
    .post(auth.logged, async (req: Request, res: Response) => {
        let editData: any = {
            social: {}
        };

        editData.name = req.body.name;
        editData.email = req.body.email;
        editData.address = req.body.address;
        editData.companyName = req.body.companyName;
        editData.social.socialFacebook = req.body.socialFacebook;
        editData.social.socialTwitter = req.body.socialTwitter;
        editData.social.socialGoogle = req.body.socialGoogle;
        editData.social.socialVk = req.body.socialVk;
        editData.emailNotify = req.body.emailNotify;

        //@ts-ignore
        if(req.files && req.files.profileImage) {

            let user = await User.item(req.session.user._id);
            removeImage(user.profileImage);

            //@ts-ignore
            editData.profileImage = uploadImage(req.files.profileImage, 'users');
            req.session.user.profileImage = editData.profileImage;
        }

        if(req.body.password) {
            await bcrypt.hash(editData.password, 10, function(err, hash) {
                editData.password = hash;
            });
        }

        await User.update(req.session.user._id, editData)
            .then(() => {
                req.session.user.name = editData.name;
                return res.send('ok');
            });
    })
    .get(auth.logged, async (req: Request, res: Response) => {
        let userData = await User.item(req.session.user._id);
        return res.render('profile/user-settings', {userData})
    });

// TODO we need it??
router.route('/purchase-history')
    .get(auth.logged, (req: Request, res: Response) => {
        Order.myItems(req.session.user._id)
            .then(item => res.render('profile/purchase-history', {Orders: item}))
    });

export default router;
