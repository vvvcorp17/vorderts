import {Router, Request, Response} from 'express';
import auth from '../../helpers/authMiddleWare';

import {removeImage, uploadImage, uploadImages} from '../../helpers/imageUpload'

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;
const router = Router();

import Provider from "../../components/provider/providerObject";
import Service from "../../components/service/serviceObject";

// access for resource Owner only
// router.route('/edit/:id')
//     .get(auth.serviceOwner, (req: Request, res: Response) => {
//         const {id} = req.params;
//         Provider.item(id)
//             .then((item) => {
//                 res.render(`providers/manage`, {item})
//             })
//             .catch((err) => res.render('errors/404'));
//     })
//     .post(auth.serviceOwner, (req: Request, res: Response) => {
//         const {id} = req.params;
//
//         let {ruTitle, ruSDesc, ruFDesc, roTitle, roSDesc, roFDesc,} = req.body;
//
//         let ru = {title: ruTitle, sDesc: ruSDesc, fDesc: ruFDesc};
//         let ro = {title: roTitle, sDesc: roSDesc, fDesc: roFDesc};
//
//         let data = {ru, ro};
//
//         //@ts-ignore
//         if(req.files && req.files.image) data.mainImage = uploadImage(req.files.image, 'providers');
//         //@ts-ignore
//         if(req.files && req.files.images) data.images = uploadImages(req.files.images, 'providers');
//
//         Provider.update(id, data)
//             .then(() => res.redirect('/providers/view/' + id))
//             .catch((err) => res.json(err));
//     });
//
// router.route('/delete/:id')
//     .get(auth.serviceOwner, (req: Request, res: Response) => {
//         const {id} = req.params;
//         Provider.delete(id)
//             .then(() => {
//                 res.end('true');
//             })
//             .catch(() => res.render('errors/404'));
//     });
//
// router.route('/delete/image/:id')
//     .post(auth.serviceOwner, (req: Request, res: Response) => {
//         const {id} = req.params;
//         let deleteImage = req.body['deleteImage'] || [];
//         removeImage(deleteImage);
//         Provider.pullImage(id, deleteImage)
//             .then(res => res.send(res))
//             .catch(err => res.send(err))
//     });
//
// // access for Logged only
// router.route('/create')
//     .get(auth.logged, (req: Request, res: Response) => {
//         res.render(`providers/manage`);
//     })
//     .post(auth.logged, (req: Request, res: Response) => {
//
//         //@ts-ignore
//         let mainImage = req.files && uploadImage(req.files.image, 'providers');
//         //@ts-ignore
//         let images    = req.files && uploadImages(req.files.images, 'providers');
//
//
//         let {ruTitle, ruSDesc, ruFDesc, roTitle, roSDesc, roFDesc,} = req.body;
//
//         let ru = {title: ruTitle, sDesc: ruSDesc, fDesc: ruFDesc};
//         let ro = {title: roTitle, sDesc: roSDesc, fDesc: roFDesc};
//
//         const user = req.session.user._id;
//         Provider.add({ru, ro, user, mainImage, images})
//             .then((item) => res.redirect('/providers/view/' + item._id))
//             .catch((err) => res.send(err))
//     });
//
//
// // access for all
//
// router.route('/list')
//     .get(async (req: Request, res: Response) => {
//
//         res.locals.query = req.query;
//         let page = req.query.page || 1;
//         let limit = 9;
//
//         let Providers = await Provider.items(limit, (limit * page) - limit);
//
//         let Count = await Provider.count();
//
//         res.locals.page = page;
//         res.locals.allCount = Math.ceil(Count / limit);
//
//         res.render(`providers/list`, {Providers});
//     });
//
// router.route('/view/:id')
//     .get((req: Request, res: Response) => {
//         const {id} = req.params;
//
//         // array of promises
//         Promise.all([
//             Provider.item(id),
//             Service.items({providerId: ObjectId(id)})
//         ]).then(values => {
//             if (values[0]) {
//                 return res.render(`providers/view`, {
//                     item: values[0],
//                     serviceList: values[1]
//                 });
//             }
//             return res.render('errors/404')
//         });
//     });


export default router;
