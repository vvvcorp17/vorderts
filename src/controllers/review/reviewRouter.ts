import {Router, Request, Response, response} from 'express';
const router = Router();
import auth, {Auth} from '../../helpers/authMiddleWare';
import mail from '../../components/mail/mailObject';

import Review from "../../components/review/reviewObject";
// import Provider from "../../components/provider/providerObject";
// import Service from "../../components/service/serviceObject";
// import User from "../../components/user/userObject";
// import {removeImage} from "../../components/helpers/imageUpload";

router.route('/serviceReview')
    .get(auth.admin, (req: Request, res: Response) => {
        Review.items({resourceType: 'mainService', resourceId: '5ed7bbe01f5bb21770296f77'})
            .then((items) => {
                return res.json(items)
            })
            .catch((err) => res.json(err))
    })
    .post((req: Request, res: Response) => {
        let {body, rate} = req.body;

        Review.add({resourceType: 'mainService', resourceId: '5ed7bbe01f5bb21770296f77', body, rate, user: req.session.user && req.session.user._id || null})
            .then((response) => res.json(response))
            .catch((err) => res.status(404).json(err))

        mail.send(
            'neoninza3@gmail.com',
            'support-vorder@gmail.com',
            'Review from ' + req.hostname,
            'Rate: ' + rate + ', Review: ' + body
        );

    });

router.route('/:resourceType/:resourceId/rate')
    .get((req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        Review.itemsAvgRate(resourceId)
            .then((rate) => {
                return res.json(rate[0])
            })
            .catch((err) => res.json(err))
    });

router.route('/:resourceType/:resourceId')
    .get((req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        Review.items({resourceType, resourceId})
            .then((items) => {
                return res.json(items)
            })
            .catch((err) => res.json(err))
    })
    .post(auth.logged, (req: Request, res: Response) => {
        let {resourceType, resourceId} = req.params;
        let {body, rate} = req.body;

        Review.add({resourceType, resourceId, body, rate, user: req.session.user._id})
            .then((response) => res.json(response))
            .catch((err) => res.status(404).json(err))

    });

// TODO make ability
// router.route('/:id')
//     .put(auth.reviewOwner, (req: Request, res: Response) => {
//         console.log('put')
//     })
//     .delete(auth.reviewOwner, (req: Request, res: Response) => {
//         Review.delete(req.params.id)
//             .then(() => res.send({status: 'ok'}))
//     });

export default router;
