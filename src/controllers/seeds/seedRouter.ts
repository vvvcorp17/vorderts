import {Router, Request, Response} from 'express';
import Faker from 'faker';
import moment from "moment";

const router = Router();

// Models
import ProviderModel from '../../components/provider/models/providerModel';
import ServiceModel from '../../components/service/models/serviceModel';
import OrdersModel from '../../components/order/models/orderModel';
import LogModel from '../../components/log/models/logModel';
import SysLogModel from '../../components/sysLog/models/sysLogModel';
import Category from '../../components/category/models/categoryModel';
import Reviews from '../../components/review/models/reviewModel';
import Comments from '../../components/comment/models/commentModel';
import Users from '../../components/user/models/userModel';

// Objects
import ServiceObject from '../../components/service/serviceObject';


import mongoose, {Schema} from 'mongoose';
import auth from "../../helpers/authMiddleWare";
let ObjectId = mongoose.Types.ObjectId;

router.route('/remove-all')
    .post(auth.admin, (req, res) => {
        Users.remove({}).then();
        ProviderModel.remove({}).then();
        ServiceModel.remove({}).then();
        OrdersModel.remove({}).then();
        Reviews.remove({}).then();
        Comments.remove({}).then();
        LogModel.remove({}).then();
        SysLogModel.remove({}).then();
        res.send('ok');
    });

// access for resource Owner only
router.route('/:count?')
    .post(auth.admin, async (req, res) => {

        let count = req.params.count+0 || 10;
        const user = req.session.user._id;

        Faker.locale = "ru";
         let ruTitle = {
            title: Faker.company.companyName(),
            sDesc: Faker.lorem.sentence(),
            fDesc: Faker.lorem.paragraphs(),
        };

        let category = await Category.findOne()
            .then(async (cat) => {
                if(cat) return cat._id;

                let object = new Category({
                    ruTitle: "Здоровье",
                    roTitle: "Sanatate",
                    enTitle: "Health"
                });
                return object.save()
            });

        for( let i = 0; i < count; i++ ) {
            ServiceObject.add(
                {
                    ruTitle: "ru" + Faker.company.companyName(),
                    ruSDesc: "ru" + Faker.lorem.sentence(),
                    ruFDesc: "ru" + Faker.lorem.paragraphs(),

                    roTitle: "ro " + Faker.company.companyName(),
                    roSDesc: "ro " + Faker.lorem.sentence(),
                    roFDesc: "ro " + Faker.lorem.paragraphs(),

                    enTitle: "en" + Faker.company.companyName(),
                    enSDesc: "en" + Faker.lorem.sentence(),
                    enFDesc: "en" + Faker.lorem.paragraphs(),
                    price: {
                        currency: 'MDL',
                        amount: Math.floor(Math.random() * Math.floor(999))
                    },
                    geoPosition: {lat: (47 + Math.random()*1) , lng: (28 + Math.random()*1)},
                    category: category,
                    image: `https://picsum.photos/350/230?random=${i+1}`,
                    workingDays:[],
                    specialists:[],
                    startTime: moment('08:00', 'HH:mm').valueOf(),
                    endTime: moment('18:00', 'HH:mm').valueOf(),
                    startLunch: moment('12:00', 'HH:mm').valueOf(),
                    endLunch: moment('13:00', 'HH:mm').valueOf(),
                    publish: true,
                    user
                }
            );
        }

        res.send("ok")

    });

export default router;
