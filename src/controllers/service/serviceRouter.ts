import {Router, Request, Response, NextFunction} from 'express';
import Service from '../../components/service/serviceObject';
import auth from '../../helpers/authMiddleWare';
const router = Router();
import {uploadImage, removeImage} from '../../helpers/imageUpload';
import Category from '../../components/category/categoryObject';
import moment from "moment";
import xssFilters from "xss-filters";

function addUpdateData(req) {
    let {ruTitle, ruSDesc, ruFDesc,
        roTitle, roSDesc, roFDesc,
        enTitle, enSDesc, enFDesc,
        publish, maxOrdersOnTime, serviceTimeDuration,
        geoPositionLat, geoPositionLng, geoLocationName,
        workingDays, startTime ,endTime ,startLunch ,endLunch, amount,
        category, currency, lat, lng, geoName,
        specialists } = req.body;

    let geoPosition = {lat: lat, lng: lng, name: geoName};

    let image = '';

    if(req.files && req.files.image) {
        image = uploadImage(req.files.image, 'services');
    }

    let data = {
        user: req.session.user._id,
        ruTitle: xssFilters.inHTMLData(ruTitle),
        ruSDesc: xssFilters.inHTMLData(ruSDesc),
        ruFDesc: ruFDesc,
        roTitle: xssFilters.inHTMLData(roTitle),
        roSDesc: xssFilters.inHTMLData(roSDesc),
        roFDesc: roFDesc,
        enTitle: xssFilters.inHTMLData(enTitle),
        enSDesc: xssFilters.inHTMLData(enSDesc),
        enFDesc: enFDesc,
        geoPosition,
        workingDays,
        specialists,
        startTime: startTime,
        endTime: endTime,
        startLunch: startLunch,
        endLunch: endLunch,
        category,
        publish,
        maxOrdersOnTime,
        serviceTimeDuration,
        price: {
            currency,
            amount
        },
    };

    if(image) data['image'] = image;

    return data;
}

router.route('/')
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {

        let data = addUpdateData(req);
        let {suggestCategory} = req.body;
        if(suggestCategory) {
            await Category.add({ruTitle: suggestCategory, roTitle: suggestCategory, enTitle:suggestCategory})
                .then(category => data.category = category._id)
                .catch((err) => console.log(err))
        }

        Service.add(data)
            .then((response) => res.status(201).json('Added'))
            .catch((err) => res.status(400).json({'err': err}));
    })
    .get(async (req: Request, res: Response) => {
        let find = req.query;
        find.reqUser = req.session.user && req.session.user._id || null;
        let lang = req.cookies.lang || 'ru';
        Service.items(find, lang)
            .then((items) => {
                Service.count(find, lang)
                    .then(count => {
                        res.json({items, count});
                    });
            })
            .catch((err) => res.status(400).json(err));

    });

router.route('/suggest')
    .get((req: Request, res: Response) => {
        const {title} = req.query;
        Service.items({title}, req.cookies.lang)
            .then(items => res.json(items))
            .catch(err => res.send(err))
    });

router.route('/related')
    .get((req: Request, res: Response) => {
        const {id, category} = req.query;
        Service.items({_id: {$ne: id}, category}, req.cookies.lang)
            .then(items => res.json(items))
            .catch(err => res.send(err))
    });

router.route('/:id')
    .get((req: Request, res: Response) => {
        const {id} = req.params;
        Service.item(id)
            .then(item => res.json(item));
    })
    .put(auth.serviceOwner, (req: Request, res: Response) => {
        const {id} = req.params;

        let data = addUpdateData(req);

        if(data['image']) {
            Service.item(id)
                .then(item => {
                    if(item.image) {
                        removeImage(item.image);
                    }
                });
        }

        Service.update(id, data)
            .then((response) => res.json(response))
            .catch((err) => res.status(400).json({'err': err}));
    })
    .delete(auth.serviceOwner, (req: Request, res: Response) => {
        const {id} = req.params;
        Service.delete(id)
            .then((item) => {
                res.json("Deleted");
            })
            .catch((err) => res.status(404).json(err));
    });

router.route('/list/geo')
    .post(async (req: Request, res: Response) => {
        Service.itemsGeo(req.body).then((items) => res.json(items));
    });


export default router;
