import {Router, Request, Response, NextFunction} from 'express';
import Specialist from '../../components/specialist/specialistObject';
import auth from '../../helpers/authMiddleWare';
import bcrypt from 'bcryptjs';

const router = Router();
import {uploadImage, uploadImages, removeImage} from '../../helpers/imageUpload'

// access for Logged only
router.route('/') //secured by filter serviceProvider
    .get( async (req: Request, res: Response, next: NextFunction) => {
        Specialist.items({serviceProvider: req.session.user._id})
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    })
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {

        if(req['files'] && req['files'].image) {
            req.body['image'] = uploadImage(req['files'].image, 'specialists');
        }

        req.body.serviceProvider = req.session.user._id;

        Specialist.add(req.body)
            .then((item) => res.json(item))
            .catch((err) => res.status(400).json({'err': err}));
    });

router.route('/:id')
    .get( async (req: Request, res: Response, next: NextFunction) => {
        Specialist.item(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.status(404).json(err))
    })
    .put(async (req: Request, res: Response, next: NextFunction) => {

        let { id } = req.params;
        let { name, email, description, position, phone} = req.body;

        var data = {
            name, email, description, position, phone
        };


        if(req['files'] && req['files'].image) {
            var image = uploadImage(req['files'].image, 'specialists');
            data['image'] = image;
        }

        Specialist.update(id, req.session.user._id, data)
            .then((item) => res.json(data))
            .catch(err => res.status(400).json(err))

    })
    .delete( async (req: Request, res: Response, next: NextFunction) => {
        Specialist.delete(req.params.id, req.session.user._id)
            .then((items) => res.json(items))
            .catch(err => res.status(400).json(err))
    });

export default router;
