import {Router, Request, Response, NextFunction} from 'express';
import User from '../../components/user/userObject';
import auth from '../../helpers/authMiddleWare';
import bcrypt from 'bcryptjs';

const router = Router();
import {uploadImageFromBase64} from '../../helpers/imageUpload'
import {IUser} from "../../components/user/models/userModel";

const fs = require("fs");

// access for Logged only
router.route('/subscribe')
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        let thisUser = req.session.user._id;

        User.subscribe(thisUser, req.body.enpoint)
            .then((items) => {
                res.json(items)
            })
            .catch(err => res.json(err))
    });

router.route('/')
    .get(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        User.items(req.query)
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    })
    .post(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        User.add(req.body)
            .then((item) => res.json(item))
            .catch((err) => res.json({'err': err}));
    });

router.route('/:id')
    .get(async (req: Request, res: Response, next: NextFunction) => {
        User.item(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    })
    .put(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        if(req.params.id !== req.session.user._id) {
            return res.status(403).json();
        }

        let user:IUser = req.body;

        if(user.image) {
            user.image = uploadImageFromBase64(user.image, user.email+'_avatar.png', 'users');
        } else {
            delete user.image;
        }

        if(user.password) {
            user.password = bcrypt.hashSync(user.password, 10);
        }

        User.update(req.session.user._id, user)
            .then((item) => res.json(user))
            .catch(err => res.status(400).json(err))

    })
    .delete(auth.logged, async (req: Request, res: Response, next: NextFunction) => {
        User.delete(req.params.id)
            .then((items) => res.json(items))
            .catch(err => res.json(err))
    });

export default router;
