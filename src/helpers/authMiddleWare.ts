import mongoose from 'mongoose';
import HTTPrequest from 'request';

import providers from '../components/provider/models/providerModel';
import services from '../components/service/models/serviceModel';
import orders from '../components/order/models/orderModel';
import comments from '../components/comment/CommentObject';
import review from '../components/review/reviewObject';

let ObjectId = mongoose.Types.ObjectId;


// Добавь сюда все модели импортированные выше
// потом будет возможность обращаться к ним через переменную в middleWare owner)
// maybe gavnoCode
const allModels = {
    providers: providers,
    services: services,
    orders: orders,
};


const methods = {

    admin: (req, res, next) => {
        if(req.session.user && req.session.user.email === "admin@vorder.online") {
            next();
        } else return res.status(403).send('')
    },

    logged: (req, res, next) => {
        if (!req.session.user) {
            return res.status(403).json();
        }
        next();
    },

    can: (resourceName, resourceId, user) => {
        return new Promise((resolve, reject) => {
            allModels[resourceName].findOne({'_id': ObjectId(resourceId), 'user': user})
                .then((provider) => {
                    resolve(!!provider);
                });
        });
    },

    commentOwner: (req, res, next) => {
        comments.itemByColumn({_id: req.params.id, user: req.session.user._id})
            .then((item) => {
                if(item) return next();
                else return res.status(403).json();
            })
    },

    reviewOwner: (req, res, next) => {
        review.itemByColumn({_id: req.params.id, user: req.session.user._id})
            .then((item) => {
                if(item) return next();
                else return res.status(403).json();
            })
    },

    serviceOwner: (req, res, next) => {

        if (req.cookies.vp) {
            next();
            return;
        }

        if(!req.session.user) return res.status(403).json();

        let {id} = req.params;

        // const user = providers.findById(id)
        const user = services.findOne({'_id': id, 'user': req.session.user._id})
            .then((provider) => {
                if (provider) {
                    next();
                    return;
                }
                return res.status(403).json()
            })
            .catch((err) => {
                return res.json(err);
            });
    }
};

export default methods;

export class Auth {
    static check(token) {
        return new Promise((resolve, reject) => {
            HTTPrequest.post(process.env.SSO_URL + '/api/getUser', {form: {token}},
                function (err, HTTPResponse, body) {
                    if (err) reject(err);
                    resolve(body);
                }
            )
        })
    }
}
