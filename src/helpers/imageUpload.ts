import fs from 'fs';
import {Schema} from "mongoose";
import {toString} from "express-validator/src/utils";
var path = require('path');
var resource = './src/public/uploads/';


export function uploadImages(files, location) {
    // Upload 1 or more files
    let images = [];
    let path = resource + location + '/';

    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }

    if(files) {
        let array_of_images = Array.isArray(files) ? files : [files];
        for (let i=0; i < array_of_images.length; i++ ) {
            images[i] = '/uploads/'+ location +'/' + new Date().getTime() + array_of_images[i].name;
            array_of_images[i].mv('./src/public' + images[i], function (err) {
                if (err) {
                    console.log(err);
                }
            });
        }
    }

    return images;
}

export function uploadImageFromBase64(image, fileName, location): String {

    let fullPath = resource + location + '/';

    if (!fs.existsSync(fullPath)){
        fs.mkdirSync(fullPath);
    }

    let base64Data = image.replace(/^data:image\/png;base64,/, "");

    fs.writeFileSync(fullPath + fileName, base64Data, 'base64');

    return '/uploads/' + location + '/' + fileName;
}


export function uploadImage(image, location) {

    if (!fs.existsSync(resource + location)){
        fs.mkdirSync(resource + location);
    }

    let imageName = '';
    if(image) {
        imageName = '/uploads/' + location + '/' + new Date().getTime() + image.name;
        image.mv('./src/public' + imageName, function (err) {
            if (err) {
                console.log(err);
            }
        });
    }

    return imageName;
}

export function removeImage(path) {
    fs.unlinkSync('./src/public' + path);
}
