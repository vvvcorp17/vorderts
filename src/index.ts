import Application from './app';
import { connect } from './database';

const app = new Application();
connect();

app.start();
