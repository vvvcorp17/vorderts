function Category() {

    let categories = null;

    this.appendTo = async (appendTo = "#category") => {
        this.appedTo = appendTo;
        await this.drawCategories();
    };

    this.drawCategories = async () => {
        let appedTo = this.appedTo;
        await this.fetchCategories();
        categories.forEach(function (item) {
            $(appedTo).append(`<option value="${item._id}" >${item[Language+"Title"]}</option>`);
        });
    };

    this.fetchCategories = async () => {

        if(categories) return categories;

        categories = await $.get("/categories", function (items) {
            return items;
        }).fail(function (mess) {
            alert(JSON.stringify(mess));
        });
    }
}