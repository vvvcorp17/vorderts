class Order {
    // myOrders
    drawOrders(order = "clientOrders", container=".orders") {
        return $.get("/orders/"+order, function (items) {
            if(items){
                items.forEach(function (item) {
                    $(container).append(order === "clientOrders" ? (new Order()).clientOrderComponent(item) : (new Order()).myOrderComponent(item))
                })
            } else {
                $(container).append("No Orders")
            }
        });
    }

    myOrderComponent (order) {
        return `
            <tr class="order-${order._id}">
                <td>
                    <a href="/service.html?id=${order.service._id}">
                        <div class="short_desc">
                            <h6>${order.service[Language+"Title"]}</h6>
                        </div>
                    </a>
                </td>
                <td>
                    <ul>
                        <li class="license">
                            <p>
                                <span translate="to-date-time"></span>: ${moment(order.params.toDataTime).format('YYYY-MM-DD hh:mm')}</p>
                        </li>
                        <li class="license">
                            <p>
                                <span translate="created"></span>: ${moment(order.created).format('YYYY-MM-DD hh:mm')}</p>
                        </li>
                    </ul>
                </td>
                <td>${order.price.amount } ${order.price.currency}</td>
                <td>
                    <div>
                        <div class="orderStatus">
                            ${order.accepted === null && '<span translate="pending"></span>' || order.accepted === true && '<span translate="accepted"></span>' || '<span translate="declined"></span>'}
                        </div>
                    </div>
                </td>
            </tr>        `
    }
    clientOrderComponent (order) {
        return `
            <tr class="order-${order._id}">
                <td>
                    <a href="/service.html?id=${order.service._id}">
                        <div class="short_desc">
                            <h6>${order.service[Language+"Title"]}</h6>
                        </div>
                    </a>
                </td>
                <td>
                    <ul>
                        <li class="license">
                            <p>
                                <span translate="to-date-time"></span> ${moment(order.params.toDataTime).format('YYYY-MM-DD hh:mm')}</p>
                        </li>
                        <li class="license">
                            <p>
                                <span translate="created"></span> ${moment(order.created).format('YYYY-MM-DD hh:mm')}</p>
                        </li>
                    </ul>
                </td>
                <td><div ><a href="/profile/profile.html?user=${order.user && order.user._id}">${order.name}</a></div></td>
                <td>${order.price.amount } ${order.price.currency}</td>
                <td>
                    <div>
                    ${order.accepted === null &&`
                        <div class="statusManage">
                                <form class="accept" action="/orders/accept/${ order._id}" onSuccess="setStatus(true, '${order._id}')" method="post">
                                    <button class="btn btn--round btn-sm" translate="accept"></button>
                                </form>
                                <form class="decline" action="/orders/decline/${order._id}" onSuccess="setStatus(false, '${order._id}')" method="post">
                                    <button class="btn btn--round btn-sm" translate="decline"></button>
                                </form>
                        </div>` || ''}
                        <div class="orderStatus">
                            ${order.accepted === null && '<span translate="pending"></span>' || order.accepted === true && '<span translate="accepted"></span>' || '<span translate="declined"></span>'}
                        </div>
                    </div>
                </td>
            </tr>        `
    }

}