class Service {
    constructor() {
        this.itemInPage = 9;
    }

    getItems(query = '') {
        return $.get("/services" + query, function (items)  {
            return items;
        });
    }

    getItem(id) {
        return $.get("/services/" + id, function (item) {
            return item;
        });
    }

    renderItems(searchParams, withPagination = false, colCount = 4) {
        loader(true, ".services");
        var renderI = this.renderItem;
        return this.getItems(searchParams)
            .done(function (res) {
                $(".services").html("");
                if(!res.items.length) {
                    $(".services").html("No items");
                    return;
                }
                (res.items).forEach(function (item, index) {
                    // setTimeout(function () {
                        renderI(item, ".services", colCount)
                    // }, 10*index);
                });

                // setTimeout(function () {
                    translate()
                // }, 10*res.items.length);

                setTimeout(function () {
                    $(".service-rate").each(function (item) {
                        let num = parseInt($(this).text());
                        if(isNaN(num)) return;
                        $(this).text('');
                        $(this).rate({
                            selected_symbol_type: 'fontawesome_star',
                            readonly: true,
                            initial_value: num,
                            max_value: 5,
                            step_size: 1,
                        });
                    })
                }, 100)

                if(!withPagination) return;

                // draw pagination

                let current = parseInt(new URL(location.href).searchParams.get("page")) || 1;
                let countPages = Math.ceil(res.count/9)
                let url = new URL(location.href);

                url.searchParams.set("page", current > 1 ? current - 1 : 1);

                let pagination =`<div class="col-md-12">
                    <div class="pagination-area">
                        <nav class="navigation pagination" role="navigation">
                            <div class="nav-links">
                                <a class="prev page-numbers" href="/services.html${url.search}">
                                    <span class="lnr lnr-arrow-left"></span>
                                </a>`;

                if(countPages <10) {
                    for (var i = 1; i <= countPages; i++) {
                        url.searchParams.set("page", i);
                        pagination += `<a class="page-numbers ${current == i ? 'current' : ''}" href="/services.html${url.search}">${i}</a>`;
                    }
                } else {
                    url.searchParams.set("page", 1);
                    pagination += `<a class="page-numbers ${current == 1 ? 'current' : ''}" href="/services.html${url.search}">${1}</a>`;
                    url.searchParams.set("page", 2);
                    pagination += `<a class="page-numbers ${current == 2 ? 'current' : ''}" href="/services.html${url.search}">${2}</a>`;
                    url.searchParams.set("page", 3);
                    pagination += `<a class="page-numbers ${current == 3 ? 'current' : ''}" href="/services.html${url.search}">${3}</a>`;
                    pagination += `..... <input onchange="goToPage($(this).val())" style=" text-align: center; width: 60px; padding: 6px;" type="number" min="1" max="${countPages}" name="page" value="${current >3 && current<countPages-3 && current}" /> ....` ;
                    url.searchParams.set("page", countPages-3);
                    pagination += `<a class="page-numbers ${current == countPages-3 ? 'current' : ''}" href="/services.html${url.search}">${countPages-3}</a>`;
                    url.searchParams.set("page", countPages-2);
                    pagination += `<a class="page-numbers ${current == countPages-2 ? 'current' : ''}" href="/services.html${url.search}">${countPages-2}</a>`;
                    url.searchParams.set("page", countPages-1);
                    pagination += `<a class="page-numbers ${current == countPages-1 ? 'current' : ''}" href="/services.html${url.search}">${countPages-1}</a>`;
                }

                url.searchParams.set("page", current >= countPages ? countPages : current + 1 );
                pagination +=` <a class="next page-numbers" href="/services.html?page=${url.search}">
                                    <span class="lnr lnr-arrow-right"></span>
                                </a>
                            </div>
                        </nav>
                    </div>
                </div>`;

                // setTimeout(function () {
                    countPages && countPages > 1 && $(".services").append(pagination);
                // }, 10*res.items.length)
            });
    }

    renderItem(service, container, colCount = 6) {
        var serviceItem = `<div class="serviceItem col-lg-${colCount} col-md-6">
                    <div class="product product--card">
                        <div class="product__thumbnail">
                            <a href="/service.html?id=${service._id}" class="product_title">
                                <img src="${service.image || '/assets/images/p1.jpg'}" alt="Product Image">
                            </a>
                            
                            ${loggedUser._id === service.user._id && `<div class="prod_option">
                                <a href="#" id="drop2" class="dropdown-trigger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span class="lnr lnr-cog setting-icon"></span>
                                </a>

                                <div class="options dropdown-menu" aria-labelledby="drop2">
                                    <ul>
                                        <li>
                                            <a href="/profile/service-manage.html?id=${service._id}">
                                                <span class="lnr lnr-pencil"></span><span translate="edit"></span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-id="${service._id}" data-toggle="modal" onclick="service.modalDeleteService(this)" data-target="#deleteModal" class="delete">
                                                <span class="lnr lnr-trash"></span><span translate="delete"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>` || `<div class="prod_btn">
                                <a href="/service.html?id=${service._id}" class="transparent btn--sm btn--round">More Info</a>
                            </div>`} 
                            
                            
                        </div>
                        <div class="product-desc">
                            <a href="/service.html?id=${service._id}" class="product_title">
                                <h4>${service[Language+'Title']}</h4>
                            </a>
                            <ul class="titlebtm">
                                <li>
                                    <img class="auth-img" src="${service.user.image || '/assets/images/m1.png'}" alt="author image">
                                    <p>
                                        <a href="/profile/profile.html?user=${service.user._id}">${service.user && service.user.name}</a>
                                    </p>
                                </li>
                                <li class="product_cat">
                                    <a href="/services.html?category=${service.category._id}">
                                        <span class="lnr lnr-book"></span>${service.category[Language+"Title"]}</a>
                                </li>
                            </ul>
                            <p>${service[Language+'SDesc']}</p>
                        </div>
                        <div class="product-purchase">
                            <div class="price_love">
                                <span>
                                    ${service.price.amount} 
                                    ${service.price.currency} 
                                </span>
                            </div>
                            <span class="lnr lnr-eye"> ${service.viewCount}</span>
                            <div class="sell">
                                <p>
                                    <div class="service-rate" style="float: left">${service.rate.avgRate}</div>
                                    (<span>${service.rate.sumRate}</span>)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>`;
        $(container).append(serviceItem);
        return true;
    }

    modalDeleteService(service) {
        if(confirm("Are you sure you want to delete this?")) {
            let id = $(service).attr("data-id");
            $.ajax({
                url: "/services/" + id,
                method: "delete",
            }).done(function (mess) {
                $('a[data-id="' + id + '"]').closest(".serviceItem").remove();
            });
        }
    }

}