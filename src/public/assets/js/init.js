(function () {
  "use strict";

  // changeURL(location.pathname);

  if (typeof jQuery == 'undefined') {
    let xhr = new XMLHttpRequest();

// 2. Настраиваем его: GET-запрос по URL /article/.../load
    xhr.open('GET', '/index.html');

    xhr.overrideMimeType('application/javascript');

// 3. Отсылаем запрос
    xhr.send();

// 4. Этот код сработает после того, как мы получим ответ сервера
    xhr.onload = function() {
      if (xhr.status != 200) { // анализируем HTTP-статус ответа, если статус не 200, то произошла ошибка
        alert(`Ошибка ${xhr.status}: ${xhr.statusText}`); // Например, 404: Not Found
      } else { // если всё прошло гладко, выводим результат
        document.querySelector("html").innerHTML = xhr.response;
        eval(xhr.responseText)
      }
    };

    xhr.onerror = function() {
      alert("Запрос не удался");
    };
  }

})();
