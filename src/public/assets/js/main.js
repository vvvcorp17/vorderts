    "use strict";
    loader(true);

    // if(location.host !== "localhost:4000"){
    //     window.onerror = function(msg, url, linenumber) {
    //         alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
    //         return true;
    //     };
    // }

    // Definitions

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var body = $("body");
    var mainWindow = window;
    var Currency = currencyGet();
    var Language = defineLangByURL();
    var loggedUser = {};
    var service = new Service();
    var fetchNewMessagesCountInterval = null;

    // Events
    $.cookie("lang") || $.cookie("lang", Language, { path : "/", expires : 30 })
    loginCheck()
        .then(function (res) {
        })
        .catch(function () {
        })
        .then(function () {
            getPage(location.pathname);
            getPage("menu.html", "#menuBlock");
            fetchNewMessagesCount();
        });

    window.onpopstate = function(event) {
        getPage(location.pathname);
    };

    moment.locale(Language);

    body.on("click", ".mob-burger, .navbar-nav li", function (e) {
        if(window.screen.width <1001) $("#menu ul").toggleClass('showFlex')
    });

    function change_language(element) {
        let prevLang = Language;
        Language = $(element).attr("data-lang");
        $.cookie("lang", Language, { path : "/", expires : 30 })
        changeURL(location.pathname.replace(defineLangByURL(), Language)+location.search)
    }

    function currencySet(currency) {
        Currency = currency;
        $.cookie("currency", currency);
        refreshPage();
    }

    function currencyGet() {
        return $.cookie("currency") || 'USD';
    }

    function defineLangByURL(href = location.pathname) {
        let path = href;
        let url = path.split("/");
        let lang = url[1];
        if(['ru', 'en', 'ro'].indexOf(lang) > -1) {
            // return lang;
            return 'ru';
        }

        // url without lang
        let newLang = $.cookie("lang") || 'ru';

        history.pushState({}, 'title', '/'+newLang+path+location.search);
        // return newLang;
        return 'ru';
    }

    body.on("click", "a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");

        if (
            // href === location.pathname ||
            href.indexOf("#") === -1
        ) changeURL(`/${Language}`+href);
    });

    body.on("change", ".serviceFilter input, .serviceFilter select", function () {
        let url = new URL(location.href);
        url.searchParams.delete("page");
        if($(this).val())
            url.searchParams.set($(this).attr("name"), $(this).val());
        else
            url.searchParams.delete($(this).attr("name"));

        history.pushState({}, 'title', url);
    });

    // logout

    $(body).on("click", "#logout", function () {
        logout();
        translate();
        loggedUser = {}
    });

    //form handler
    body.on("submit", "form", function (e) {
        e.preventDefault();

        var form = $(this);
        if(form.attr('onsubmit')) return;

        if(!form.attr("method") || form.attr("method") == 'GET') {
            changeURL(form.attr("action") + '?' + form.serialize());
            return;
        }

        var formData = null;
        var enctypeMultipart = null;

        if(form.attr("enctype") === "multipart/form-data" ) {
            enctypeMultipart = true;
            formData = new FormData(form[0]);
        } else formData = form.serializeArray();


        $.ajax({
            url: form.attr("action") || location.href,
            method: form.attr("method"),
            data: formData,
            contentType: enctypeMultipart ? false : "application/x-www-form-urlencoded",
            processData: !enctypeMultipart,
        }).done(function(res) {
            if(form.attr('onSuccess')) {
                eval(form.attr('onSuccess'));
                // return;
            } else alertMessage('Success');

            var redirectTo = form.attr('redirectTo');
            if(redirectTo) {
                changeURL(redirectTo)
            }

        }).fail(function (res) {
            if(res.status == 403) changeURL('login.html');
            alertMessage(res.statusText, 'error');
        })

    });

    // body.on("click", ".has_dropdown", function (e) {
    //     $(this).children(".dropdowns").toggle();
    // });

    $(body).on("change", "#image", function() {
        imagePreview(this);
    });
    //
    // $(body).on("change", "#images", function() {
    //     imagesPreview(this);
    // });
    //
    // $(body).on("click", "#imagesPreview .img", function() {
    //     $(this).remove();
    // });

    // Functions


    function renderData(str) {
        let myRegexp = /(?:)<%IF(.*?)%>([\s\S]*?)<%ENDIF%>(?:|$)/g;
        let match = myRegexp.exec(str);
        let newStr = str;
        while (match != null) {
            if(eval(match[1])) newStr = newStr.replace(match[0], match[2])
            else  newStr = newStr.replace(match[0], '')
            match = myRegexp.exec(str);
        }
        return renderVariables(newStr);
    }


    function renderVariables(str) {
        let myRegexp = /(?:)<%(.*?)%>(?:|$)/g;
        let match = myRegexp.exec(str);
        let newStr = str;
        while (match != null) {
                newStr = newStr.replace(match[0], eval(match[1]))
                match = myRegexp.exec(str);
            }
        return newStr;
    }

    // function vvvComponent() {
    //     setTimeout(function () {
    //         $("[condition]").each(function () {
    //             let el = $(this);
    //             if(eval(el.attr("condition"))){
    //                 let myRegexp = /(?:)<%(.*?)%>(?:|$)/g;
    //                 let match = myRegexp.exec(el.html());
    //                 while (match != null) {
    //                     el.html(el.html().replace(match[0], eval(match[1])))
    //                     match = myRegexp.exec(el.html());
    //                 }
    //                 $(this).show();
    //             }
    //             else $(this).hide();
    //         })
    //     }, 0);
    // }

    function translate(lang = Language) {
        setTimeout(function () {
            $("#langImg").attr("src", $("#"+lang+"Img").attr("src"));
            $("[translate]").each(function() {
                let ts = $(this).attr("translate");
                if(!ts) return;

                let translatedStr = translateAll[lang][ts] || alert(ts + " is not found in translate list");
                if($(this).attr("translateElement")) {
                    $(this).attr($(this).attr("translateElement"), translatedStr)
                } else $(this).html(translatedStr)
            });
        });
    }

    function fetchNewMessagesCount() {
        $.get("/messages/getNewMessagesCount", function (count) {
            if(count) $(".author-area .notification_count").show().text(count);
        });
    }

    // function imagesPreview(input, placeToInsertImagePreview = '#imagesPreview') {
    //     $(placeToInsertImagePreview + ' img').remove();
    //     if (input.files) {
    //         var filesAmount = input.files.length;
    //
    //         for (var i = 0; i < filesAmount; i++) {
    //             var reader = new FileReader();
    //
    //             reader.onload = function(event) {
    //                 $(placeToInsertImagePreview).append(`
    //                     <div class="col-3 img"><img width="50px" src="${event.target.result}" height="50px" style="width:50px; margin-left: 5px"></div>
    //                 `);
    //               };
    //
    //             reader.readAsDataURL(input.files[i]);
    //         }
    //     }
    // }
    //
    function imagePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }

        let formData = new FormData();
        formData.append('image', input.files[0]);
    }

    function refreshPage() {
        changeURL(location.pathname+location.search)
    }

    function changeURL(href, container = '#main') {
        if(href === '/' || href === '/index.html') href = 'home.html';
        history.pushState({}, 'title', href);
        getPage(href, container);
    }

    function goToPage(num) {
        let url = new URL(location.href);
        url.searchParams.set('page', num);
        changeURL('services.html'+url.search)
    }

    function getPage(href = "/home.html", container="#main") {
        scrollTop();
        loader(true);
        href = href.replace('/'+defineLangByURL(), '');
        if(!href || href === '/') href = "/home.html";

        return $.get('/pages/' + href)
            .done(function(res) {
                $(container).html(renderData(res));
                translate(Language);
                loader(false);
                // vvvComponent();
            }).fail(function(data, textStatus, xhr) {
                $(container).html(data.responseText)
            });
    }

    // Login
    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    function logout() {
        $.get("/logout")
            .done(function (res) {
                loginCheck();
                clearInterval(fetchNewMessagesCountInterval)
                getPage("menu.html", "#menuBlock")
                changeURL("/home.html");
            })
            .fail(function () {
                alertMessage('error', 'error');
            })
    }

    function loginCheck() {
        return $.get("/check-auth")
            .done(function (res) {
                loggedUser = res || {};
                // resolve(res);
                fetchNewMessagesCountInterval = setInterval(fetchNewMessagesCount, 30000);
            })
            .fail(function () {
                loggedUser = {};
                //reject();
                // userAreaDraw();
            })
    }
    //
    // function userAreaDraw(data = null) {
    //     let userArea = '';
    //     if(data && data.length) {
    //         userArea = `<div class="userBlock">
    //                         <div class="author-area">
    //                             <div class="author-author__info inline has_dropdown" onclick="toggleLeftMenu()">
    //                                 <div class="author__avatar" >
    //                                     <span class="notification_count msg" style=""></span>
    //                                     <img width="50px" height="50px"  src="${data.image || '/assets/images/m1.png'}" alt="user avatar">
    //                                 </div>
    //
    //                                 <a href="#" id="userAreMenu" class="name dropdown-toggle btn btn--bordered btn-md custom-dropdown">
    //                                        ${data.name}
    //                                     </a>
    //                             </div>
    //                             <!--end /.author-author__info-->
    //                         </div>
    //                     </div>`;
    //     } else {
    //         userArea = `<div style="    width: max-content;    float: right;    padding-top: 7px;">
    //                                     <a href="/login.html" style="margin-right: 0;" class="author-area__seller-btn inline" translate="index_login">Login</a>
    //                                 </div>`;
    //     }
    //
    //     $(".loginBlock").html(userArea);
    // }

    function toggleLeftMenu() {
        $('.sidebar-mob').toggle()
    }

    function alertMessage(message, messageType = 'success') {
        var toastMessage = `<div class="toast" role="alert" data-delay="5000" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <span class="alert_icon lnr ${messageType === 'success' && 'lnr-checkmark-circle' || 'lnr-warning' }"></span>
                <strong class="mr-auto">Alert</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                ${message}
            </div>
        </div>`;

        $('.alert').append(toastMessage);
        $('.toast').last().toast('show');
    }

    function addNameOnEdit(el) {
        if (!$(el).attr("id")) alert("addNameOnEdit The ID is required");
        if ($(el).val()) $(el).attr("name", $(el).attr("id"))
        else $(el).removeAttr("name")
    }

    function loader(enable = true, container = "#main") {
        if (enable) $(container).html('<div class="loader"></div>')
        else $(".loader").remove()
    }

    // custom nav trigger function for owl casousel
    function customTrigger(slideNext, slidePrev, targetSlider) {
        $(slideNext).on('click', function () {
            targetSlider.trigger('next.owl.carousel');
        });
        $(slidePrev).on('click', function () {
            targetSlider.trigger('prev.owl.carousel');
        });
    }

    /* MOBILE MENU JS*/
    function mobileMenu(triggerElem, dropdown) {
        var $dropDownTrigger = $(triggerElem + ' > a');

        $dropDownTrigger.append('<span class="lnr lnr-plus-circle"></span>');
        $dropDownTrigger.find('span').on('click', function (e) {
            e.preventDefault();
            $(this).parents(triggerElem).find(dropdown).slideToggle().parents(triggerElem).siblings().find(dropdown).slideUp();
        });
    }

    // Components

    function scrollTop() {
        $('html, body').animate({ scrollTop: 0 }, 100);
    }

    var promptEvent;
    var swRegistration;
    var endpoint;


    function sw() {
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            navigator.serviceWorker.register('/sw.js')
                .then(function(swReg) {
                    console.log('Service Worker is registered', swReg);
                    swRegistration = swReg;
                })
                .catch(function(error) {
                    console.error('Service Worker Error', error);
                });
        } else {
            console.warn('Push messaging is not supported');
        }
    }

    sw();

    function urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

    var publicVapidKey = 'BGLsZTvc7aA5XwDgLN1JcDhs5U_WsrxT5S8SdXHYsKJ1GcWvecHsYGoSy3hEIJIA4c-7_cddnJKroa91mMY8fAc';

    function subscribe(element) {
        if($(element).prop("checked") === false)
            $.post('/users/subscribe', {enpoint: null});
        else {
            swRegistration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: publicVapidKey
            })
                .then(function(subscription) {
                    console.log('User is subscribed.', subscription);
                    $.post('/users/subscribe', {enpoint: JSON.stringify(subscription)});
                })
                .catch(function(err) {
                    $(element).prop("checked", false)
                    alert('Notifications are disabled. You must manually enable it');
                });
        }
    }


    // Capture event and defer
    function installAppBtn() {
        window.addEventListener('beforeinstallprompt', function (e) {
            e.preventDefault();
            promptEvent = e;

            if(promptEvent)
                $(".install-btn").show();
        });
        return '';
    }

    // present install prompt to user
    function presentAddToHome() {
        promptEvent.prompt();  // Wait for the user to respond to the prompt
        promptEvent.userChoice
            .then(choice => {
                if (choice.outcome === 'accepted') {
                    console.log('User accepted');
                } else {
                    console.log('User dismissed');
                }
            })
    }
