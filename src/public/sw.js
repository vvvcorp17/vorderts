/*
*
*  Push Notifications codelab
*  Copyright 2015 Google Inc. All rights reserved.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      https://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License
*
*/

/* eslint-env browser, serviceworker, es6 */

'use strict';

var actionURL;

/* eslint-disable max-len */

const applicationServerPublicKey = 'BPZRkIo1n2Cl3ngHo-wm56Dv6lELEzEV4QPE4mXNpYG-8Fv2K_-xDgL21NgemFK472pwqcp1x3iv4Fg3Fb8OF-o';

/* eslint-enable max-len */

const OFFLINE_URL = '/offline';


self.addEventListener('install', function(event) {
  console.log('[Service Worker] Installing Service Worker ...', event);
  event.waitUntil(
      caches.open('static').then(function(cache) {
        cache.addAll(['/']);
      })
  );
});

self.addEventListener('activate', function(event) {
  console.log('[Service Worker] Activating Service Worker ....', event);
});

self.addEventListener('fetch', function(event) {

  if (event.request.mode === 'navigate') {
    return event.respondWith(
        fetch(event.request).catch(() => caches.match(OFFLINE_URL))
    );
  }
});


function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

self.addEventListener('push', function(event) {
    let showNotification = true;
    let notificationData = event.data.json();
    actionURL = notificationData.url;

    self.clients.matchAll()
        .then(function (clients) {

            clients.forEach(function (client) {
                if(client.visibilityState === "visible" && (client.url).indexOf("messages.html") !== -1) {
                    showNotification = false;
                }

                console.log(client)
                console.log(showNotification)

            });

            if(showNotification) {
                event.waitUntil(
                    self.registration.showNotification(notificationData.title, {
                        body: notificationData.body,
                        icon: notificationData.icon,
                        //image: notificationData.image,
                        badge: notificationData.badge,
                        vibrate: notificationData.vibrate,
                        actions: notificationData.actions,
                        timestamp: notificationData.timestamp,
                    })
                )
            }
        });
});

self.addEventListener('notificationclick', function(event) {
    if (event.action === 'viewMessages') {
        clients.openWindow(actionURL);
    } else clients.openWindow(actionURL);
    event.notification.close();
}, false);


self.addEventListener('pushsubscriptionchange', function(event) {
  console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  event.waitUntil(
    self.registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
    .then(function(newSubscription) {
      // TODO: Send to application server
      console.log('[Service Worker] New subscription: ', newSubscription);
    })
  );
});
